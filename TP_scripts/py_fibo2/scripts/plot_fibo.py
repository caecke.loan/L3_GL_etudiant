#! /usr/bin/env python3

import fibo as f

import matplotlib.pyplot as plt

x = list(range(10))
y = [f.fiboIterative(i) for i in x]

plt.plot(x, y)
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('out.png')

