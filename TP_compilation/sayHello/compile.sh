#compile sayhello
g++ -std=c++11 -Wall -Wextra -c -o sayHello_solo.out sayHello.cpp 
#compile module1
g++ -std=c++11 -Wall -Wextra -c -o module1_solo.out module1.cpp
#link all
g++ -std=c++11 -Wall -Wextra -o main.out sayHello_solo.out module1_solo.out