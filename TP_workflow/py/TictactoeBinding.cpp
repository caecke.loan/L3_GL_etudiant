#include "../cpp/Tictactoe.hpp"

#include <boost/python.hpp>

BOOST_PYTHON_MODULE( Tictactoe ) {

    boost::python::enum_<Joueur>("Joueur")
        .value("VIDE", JOUEUR_VIDE)
        .value("ROUGE", JOUEUR_ROUGE)
        .value("VERT", JOUEUR_VERT)
        .value("EGALITE", JOUEUR_EGALITE)
        ;

    boost::python::class_<Jeu>("Jeu", boost::python::init<>())
        .def("raz", &Jeu::raz)
        // ...
        ;

}

